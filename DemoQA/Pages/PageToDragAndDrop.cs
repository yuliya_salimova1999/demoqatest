﻿using OpenQA.Selenium;

namespace DemoQA.Pages
{
    class PageToDragAndDrop : BasePage
    {
        public PageToDragAndDrop(IWebDriver driver) : base(driver)
        {
        }
        public IWebElement DraggableElement => _driver.FindElement(By.Id("draggable"));
        public IWebElement DroppableElement => _driver.FindElement(By.Id("droppable"));
        public IWebElement DroppableElementText => _driver.FindElement(By.XPath("//*[@id = 'droppable']/p"));
    }
}
