﻿using OpenQA.Selenium;

namespace DemoQA.Pages
{
    class PageToClickDroppable : BasePage
    {
        public PageToClickDroppable(IWebDriver driver) : base(driver)
        {
        }
        public IWebElement DroppableLink => _driver.FindElement(By.LinkText("Droppable"));
    }
}
