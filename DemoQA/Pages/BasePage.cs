﻿using OpenQA.Selenium;

namespace DemoQA.Pages
{
    class BasePage
    {
        protected IWebDriver _driver;
        public BasePage(IWebDriver driver)
        {
            _driver = driver;
        }
        public IWebElement Logo => _driver.FindElement(By.Id("logo-events"));
        public IWebElement ContentTitle => _driver.FindElement(By.ClassName("entry-title"));
    }
}
