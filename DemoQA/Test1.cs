using DemoQA.Pages;
using NUnit.Framework;
using OpenQA.Selenium.Interactions;

namespace DemoQA
{
    public class Tests : BaseTest
    {
        [SetUp]
        public void Setup()
        {
            const string MainUrl = "https://demoqa.com/";
            Driver.Navigate().GoToUrl(MainUrl);
        }

        [Test]
        public void Test1()
        {
            PageToClickDroppable pageToClickDroppable = new PageToClickDroppable(Driver);
            pageToClickDroppable.DroppableLink.Click();
            Assert.AreEqual("Droppable", pageToClickDroppable.ContentTitle.Text);
            PageToDragAndDrop pageToDragAndDrop = new PageToDragAndDrop(Driver);
            new Actions(Driver)
                .DragAndDrop(pageToDragAndDrop.DraggableElement, pageToDragAndDrop.DroppableElement)
                .Build()
                .Perform();
            StringAssert.Contains("ui-state-highlight", pageToDragAndDrop.DroppableElement.GetAttribute("class"));
            Assert.AreEqual("Dropped!", pageToDragAndDrop.DroppableElementText.Text);
        }
    }
}