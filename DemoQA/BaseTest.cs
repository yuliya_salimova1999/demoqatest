﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;

namespace DemoQA
{
    [TestFixture]
    public class BaseTest : IDisposable
    {
        protected IWebDriver Driver { get; }
        public BaseTest()
        {
            Console.Out.WriteLine("Starting the browser...");
            Driver = new ChromeDriver();
        }

        [TearDown]
        public void Dispose()
        {
            Driver.Quit();
        }
    }
}
